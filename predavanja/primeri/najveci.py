'''
ulaz: niz a i njegov broj elemenata n
izlaz: indeks najveceg elementa niza a
'''

def max(a, n):
    j = 0
    m = a[j]
    i = j+1
    while i<n:      # proveriti sve ostale elemente niza
        if m < a[i]:  # ako je tekuci element veci od prethodno najveceg
            m = a[i]  # sacuvati vrednost
            j = i     # sacuvati indeks
        i = i +1   # preci na sledeci element
    return j       # vratiti indeks najvećeg elementa
